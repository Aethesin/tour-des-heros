import { Hero } from './hero';

export const HEROES: Hero[] = [
  { id: 11, name: 'The Doctor' },
  { id: 12, name: 'Tony Stark' },
  { id: 13, name: 'Steve Roger' },
  { id: 14, name: 'Wanda Maximov' },
  { id: 15, name: 'Amy Pond' },
  { id: 16, name: 'Rory Williams' },
  { id: 17, name: 'River Song' },
  { id: 18, name: 'Peter Quill' },
  { id: 19, name: 'Monkey D. Luffy' },
  { id: 20, name: 'Gol D. Roger' }
];
