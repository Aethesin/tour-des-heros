import { Injectable } from '@angular/core';
import { InMemoryDbService } from 'angular-in-memory-web-api';
import { Hero } from './hero';

@Injectable({
  providedIn: 'root',
})
export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const heroes = [
      { id: 11, name: 'The Doctor' },
      { id: 12, name: 'Tony Stark' },
      { id: 13, name: 'Steve Roger' },
      { id: 14, name: 'Wanda Maximov' },
      { id: 15, name: 'Amy Pond' },
      { id: 16, name: 'Rory Williams' },
      { id: 17, name: 'River Song' },
      { id: 18, name: 'Peter Quill' },
      { id: 19, name: 'Monkey D. Luffy' },
      { id: 20, name: 'Gol D. Roger' }
    ];
    return {heroes};
  }

  // Overrides the genId method to ensure that a hero always has an id.
  // If the heroes array is empty,
  // the method below returns the initial number (11).
  // if the heroes array is not empty, the method below returns the highest
  // hero id + 1.
  genId(heroes: Hero[]): number {
    return heroes.length > 0 ? Math.max(...heroes.map(hero => hero.id)) + 1 : 11;
  }
}
